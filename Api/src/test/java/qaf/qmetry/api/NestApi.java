package qaf.qmetry.api;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;

public class NestApi {
	
	@Test
	public void getLeaves()
	{
		WsStep.userRequests("get.leave");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveStatus("OK");
		int statusCode = new RestTestBase().getResponse().getStatus().getStatusCode();
		String body = new RestTestBase().getResponse().getMessageBody();
		
		Reporter.log("Status code is = "+statusCode +body);
		Validator.verifyTrue(statusCode==200,"Leaves details not found","Leaves Details");
		
	}
	
	@Test
	public void getSupervisorName()
	{
		WsStep.userRequests("get.supervisorname");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveStatus("OK");
		int statusCode = new RestTestBase().getResponse().getStatus().getStatusCode();
		String body = new RestTestBase().getResponse().getMessageBody();
		
		Reporter.log("Status code is = "+statusCode +body);
		Validator.verifyTrue(statusCode==200,"Supervisor details not found","Supervisor Details found");
		
	}
	
	@Test
	public void getLocationHoliday()
	{
		WsStep.userRequests("get.locationholiday");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveStatus("OK");
		int statusCode = new RestTestBase().getResponse().getStatus().getStatusCode();
		String body = new RestTestBase().getResponse().getMessageBody();
		
		Reporter.log("Status code is = "+statusCode +body);
		Validator.verifyTrue(statusCode==200,"Location holiday details not found","Location holiday Details found");
		
	}
	
	@Test
	public void getTravelLocation()
	{
		WsStep.userRequests("get.travellocation");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveStatus("OK");
		int statusCode = new RestTestBase().getResponse().getStatus().getStatusCode();
		String body = new RestTestBase().getResponse().getMessageBody();
		
		Reporter.log("Status code is = "+statusCode +body);
		Validator.verifyTrue(statusCode==200,"Travel Location details not found","Travel Location Details found");
		
	}
	
	
	@Test
	public void getContactDeatils()
	{
		WsStep.userRequests("get.contactdetails");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveStatus("OK");
		int statusCode = new RestTestBase().getResponse().getStatus().getStatusCode();
		String body = new RestTestBase().getResponse().getMessageBody();
		
		Reporter.log("Status code is = "+statusCode +body);
		Validator.verifyTrue(statusCode==200,"Contact details not found","Contact Details found");
		
	}

}
