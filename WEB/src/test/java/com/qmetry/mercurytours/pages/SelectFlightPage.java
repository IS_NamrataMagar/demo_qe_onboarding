package com.qmetry.mercurytours.pages;

import java.util.List;

import com.qmetry.mercurytours.components.SelectFlightComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class SelectFlightPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	@FindBy(locator="SelectFlightPage.continue.btn")
	QAFWebElement continueSelectFlightBtn;
	
	@FindBy(locator = "SelectFlightPage.selectflighttitle.txt")
    private QAFWebElement selectflighttitle;
	
	 @FindBy(locator = "SelectFlightPage.departreturnrow.row")
	 public List<SelectFlightComponent> flight;
	
	public QAFWebElement getContinueSelectFlight() {
		return continueSelectFlightBtn;
	}

	public QAFWebElement getSelectflighttitle() {
		return selectflighttitle;
	}

	public List<SelectFlightComponent> getFlightBook() {
		return flight;
	}
	
	public void clickOnSelectFlight()
	{
		getContinueSelectFlight().click();
	} 

	public void clickOnDepart(String flightName) {
		List<SelectFlightComponent> listFlight = getFlightBook();

		for (SelectFlightComponent lst : listFlight) {
			if (lst.getSelecttitle().getText().equalsIgnoreCase(flightName)) {
				Reporter.log("flight name=" + lst.getSelecttitle().getText());
				lst.getRadiobutton().click();
				break;
			}
		}
		
	}

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}

}
