package com.qmetry.mercurytours.pages;

import com.qmetry.mercurytours.beans.RegisterFormDataBean;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class RegisterPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	@FindBy(locator="register.firstname.txt")
	QAFWebElement fname;
	
	@FindBy(locator="register.lastname.txt")
	QAFWebElement lname;
	
	@FindBy(locator="register.phone.txt")
	QAFWebElement phonenumber;
	
	@FindBy(locator="register.email.txt")
	QAFWebElement emailAddress;
	
	@FindBy(locator="register.address.txt")
	QAFWebElement address;
	
	@FindBy(locator="register.city.txt")
	QAFWebElement city;
	
	@FindBy(locator="register.state.txt")
	QAFWebElement state;
	
	@FindBy(locator="register.postalcode.txt")
	QAFWebElement postalCode;
	
	@FindBy(locator="register.country.drop")
	QAFWebElement country;
	
	@FindBy(locator="register.username.txt")
	QAFWebElement username;
	
	@FindBy(locator="register.password.txt")
	QAFWebElement passwordregister;
	
	@FindBy(locator="register.submit.btn")
	QAFWebElement register;
	
	@FindBy(locator="register.messagereg.text")
	QAFWebElement registermessage;
	
	
	public QAFWebElement getFirstName()
	{
		return fname;
	}
	
	public QAFWebElement getLastName()
	{
		return lname;
	}
	
	public QAFWebElement getPhoneNumber()
	{
		return phonenumber;
	}
	
	public QAFWebElement getEmailAddress()
	{
		return emailAddress;
	}
	
	public QAFWebElement getAddress()
	{
		return address;
	}

	
	public QAFWebElement getCity()
	{
		return city;
	}
	
	public QAFWebElement getState()
	{
		return state;
	}
	
	public QAFWebElement getPostalCode()
	{
		return postalCode;
	}
	
	public QAFWebElement getCountry()
	{
		return country;
	}
	
	public QAFWebElement getUserName()
	{
		return username;
	}
	
	public QAFWebElement getPasswordRegister()
	{
		return passwordregister;
	}
	
	public QAFWebElement getRegister()
	{
		return register;
	}
	
	public QAFWebElement getRegisterMessage()
	{
		return registermessage;
	}
	
	public void registerUser()
	{
		
		HomePage home = new HomePage();
		home.getResigterLink().click();
		RegisterFormDataBean bean = new RegisterFormDataBean();
		bean.fillFromConfig("data.register.user");
		bean.fillUiElements();
		Reporter.logWithScreenShot("Register Details are filled");
		RegisterPage rp = new RegisterPage();
		rp.getRegister().click();
	}
	
	public void invalidRegisterUser(String firstname,String lastname,String phone,String email)
	{
		HomePage home = new HomePage();
		home.getResigterLink().click();
		getFirstName().sendKeys(firstname);
		getLastName().sendKeys(lastname);
		getPhoneNumber().sendKeys(phone);
		getEmailAddress().sendKeys(email);
		Reporter.logWithScreenShot("All Invaild data");
		getRegister().click();
		
		Reporter.logWithScreenShot("See msg as follow:");
	}
	
	
	public void waitforpagetoLoad()
	{
		if(getRegisterMessage().verifyVisible())
		{
			Reporter.log("You are on Register Page");
		}
		else
		{
			Reporter.log("You are not on Register Page");
		}

	}	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}

}
