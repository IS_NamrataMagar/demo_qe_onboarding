package com.qmetry.mercurytours.pages;

import org.testng.Reporter;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SignOnPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator ="signon.header.text")
	QAFWebElement signonheader;
	
	@FindBy(locator ="signin.user.txtfield")
	QAFWebElement user;
	
	@FindBy(locator ="signon.password.textfield")
	QAFWebElement pass;
	
	public QAFWebElement getSignOnHeader()
	{
		return signonheader;
	}
	
	public QAFWebElement getUser()
	{
		return user;
	}
	
	public QAFWebElement getPass()
	{
		return pass;
	}
	
	
	public void waitforpagetoloadSignon()
	{
		getSignOnHeader().waitForVisible();
		getSignOnHeader().verifyVisible();
		Reporter.log("You are on Sign on Page");	
	}
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
		
	}
}
