package com.qmetry.mercurytours.pages;

import com.qmetry.mercurytours.beans.BookAFlightFormDataBean;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class BookingFlightPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator="bookflight.header.img")
	QAFWebElement bookaflightheader;
	
	@FindBy(locator="bookaflight.securepurchase.btn")
	QAFWebElement securePurchasebtn;
	
	@FindBy(locator= "bookFlight.summary.txt")
    private QAFWebElement summary;
	
	@FindBy(locator="bookaflight.firstname.text")
	QAFWebElement firstname;
	
	@FindBy(locator="bookaflight.lastname.text")
	QAFWebElement lastname;
	
	@FindBy(locator="bookaflight.meal.select")
	QAFWebElement meal;
	
	@FindBy(locator="bookaflight.cardtype.select")
	QAFWebElement cardtype;
	
	@FindBy(locator="bookaflight.cardnumber.text")
	QAFWebElement cardnumber;
	
	@FindBy(locator="bookaflight.addressbill.text")
	QAFWebElement addressbill;
	
	@FindBy(locator="bookaflight.citybill.text")
	QAFWebElement citybill;
	
	@FindBy(locator="bookaflight.statebill.text")
	QAFWebElement statebill;
	
	@FindBy(locator="bookaflight.pincodebill.text")
	QAFWebElement pincodebill;
	
	@FindBy(locator="bookaflight.countrybill.select")
	QAFWebElement countrybill;
	
	@FindBy(locator="bookaflight.addressdel.text")
	QAFWebElement addressdel;
	
	@FindBy(locator="bookaflight.citydel.text")
	QAFWebElement citydel;
	
	@FindBy(locator="bookaflight.statedel.text")
	QAFWebElement statedel;
	
	@FindBy(locator="bookaflight.pincodedel.text")
	QAFWebElement pincodedel;
	
	@FindBy(locator="bookaflight.countrydel.select")
	QAFWebElement countrydel;
	
	@FindBy(locator="bookaflight.expmonth.select")
	QAFWebElement expmonth;
	
	@FindBy(locator="bookaflight.expyear.select")
	QAFWebElement expyear;
	
	@FindBy(locator="flightconfirmation.flightconfirm.text" )
	QAFWebElement flightconfirmation;
	
	public QAFWebElement getBookaFlightHeader()
	{
		return bookaflightheader;
	}
	
	public QAFWebElement getFirstName()
	{
		return firstname;
	}
	
	public QAFWebElement getLastName()
	{
		return lastname;
	}
	
	public QAFWebElement getMeal()
	{
		return meal;
	}
	
	
	public QAFWebElement getCardType()
	{
		return cardtype;
	}
	
	public QAFWebElement getCardNumber()
	{
		return cardnumber;
	}
	
	public QAFWebElement getAddressBill()
	{
		return addressbill;
	}
	
	public QAFWebElement getCityBill()
	{
		return citybill;
	}
	
	public QAFWebElement getStateBill()
	{
		return statebill;
	}
	
	public QAFWebElement getPincodeBill()
	{
		return pincodebill;
	}
	
	public QAFWebElement getCountryBill()
	{
		return countrybill;
	}
	
	public QAFWebElement getAddressDel()
	{
		return addressdel;
	}
	
	public QAFWebElement getCityDel()
	{
		return citydel;
	}
	
	public QAFWebElement getStateDel()
	{
		return statedel;
	}
	
	public QAFWebElement getPincodeDel()
	{
		return pincodedel;
	}
	
	public QAFWebElement getCountryDel()
	{
		return countrydel;
	}
	
	public QAFWebElement getExpriyMonth()
	{
		return expmonth;
	}

	public QAFWebElement getExpiryYear()
	{
		return expyear;
	}
	
	public QAFWebElement getSecurePurchase()
	{
		return securePurchasebtn;
	}
	
	public QAFWebElement getsummary() 
	{
        return summary;
    }
	
	public String validatesummary() 
	{
        return getsummary().getText();
    }
	
	public QAFWebElement getConfirmation()
	{
		return flightconfirmation;
	}
	
	public void verifyPassengerDetails()
	{
		BookAFlightFormDataBean bean = new BookAFlightFormDataBean();
		bean.fillFromConfig("passenger.userdata1");
		bean.fillUiElements();
	}
	
	public void validationOnName(String fname,String lname)
	{
		getFirstName().sendKeys(fname);
		getLastName().sendKeys(lname);
	}
	
	public void validationOnCard(String cardType,String cardnumber)
	{
		getCardType().sendKeys(cardType);
		getCardNumber().sendKeys(cardnumber);
		
	}
	
	public void validationOnExpiration(String cardType,String cardnumber,String month,String year)
	{
		getCardType().sendKeys(cardType);
		getCardNumber().sendKeys(cardnumber);
		getExpriyMonth().sendKeys(month);
		getExpiryYear().sendKeys(year);
	}
	
	public void clickOnSecurePurchase()
	{
		getSecurePurchase().click();
		Reporter.logWithScreenShot("Verified the Flight Confirmation Page");
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	public void waitforpagetoLoad()
	{
		if(getBookaFlightHeader().verifyVisible())
		{
			Reporter.log("You are on Book A Flight Confirmation Page");
		}
		else
		{
			Reporter.log("You are not on Book A Flight Confirmation Page");
		}

	}	
	

}
