package com.qmetry.mercurytours.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FlightConfirmationPage  extends WebDriverBaseTestPage<WebDriverTestPage>{


	@FindBy(locator="flightconfrimation.logout.img")
	QAFWebElement logout;
	
	@FindBy(locator="flightconfirmation.backtoflights.btn")
	QAFWebElement backflightbtn;
	
	@FindBy(locator="flightconfirmation.flightconfirm.text")
	QAFWebElement confirmationSummary;
	
	@FindBy(locator="flightconfirmation.confirmationtext.text")
	QAFWebElement confirmationtext;
	
	public QAFWebElement getConfirmationText()
	{
		return confirmationtext;
	}
	
	public QAFWebElement getConfirmationSummary()
	{
		return confirmationSummary;
	}
	
	public QAFWebElement getLogout()
	{
		return logout;
	}
	
	public QAFWebElement getBackFlightbtn()
	{
		return backflightbtn;
	}
	
	public void clickOnLogout()
	{
		getLogout().click();
	}
	
	public void clickOnBackBtn()
	{
		getBackFlightbtn().click();
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	public void waitforpagetoLoad()
	{
		if(getConfirmationText().verifyVisible())
		{
			Reporter.log("You are on Flight Confirmation Page");
		}
		else
		{
			Reporter.log("You are not on Flight Confirmation Page");
		}

	}	
	

}
