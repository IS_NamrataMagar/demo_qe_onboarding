package com.qmetry.mercurytours.pages;

import org.testng.Reporter;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ContactPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator ="contactpage.header1.text")
	QAFWebElement contactHeader;

	public QAFWebElement getContactHeader()
	{
		return contactHeader;
	}
	
	public void waitforpagetoLoad()
	{
		if(getContactHeader().verifyVisible())
		{
			Reporter.log("You are on Contact Page");
		}
		else
		{
			Reporter.log("You are not on Contact Page");
		}

	}	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
		
	}
}
