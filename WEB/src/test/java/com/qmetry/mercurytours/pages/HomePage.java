package com.qmetry.mercurytours.pages;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.StringMatcher;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage>{

	
//	Home header
	@FindBy(locator="homepage.homeheader.img")
	QAFWebElement homeheader;
	
//	Left Component
	@FindBy(locator ="homepage.home.link")
	QAFWebElement homelink;
	
	@FindBy(locator ="homepage.flights.link")
	QAFWebElement flightLink;
	
	@FindBy(locator ="homepage.hotels.link")
	QAFWebElement hotelsLink;

	@FindBy(locator ="homepage.carrental.link")
	QAFWebElement carrentalLink;
	
	@FindBy(locator ="homepage.cruises.link")
	QAFWebElement cruisesLink;
	
	@FindBy(locator ="homepage.destinations.link")
	QAFWebElement destinationLink;
	
	@FindBy(locator ="homepage.vactionslink.link")
	QAFWebElement vactionsLink;
	
//	Top Component
	
	@FindBy(locator ="homepage.signon.link")
	QAFWebElement signOn;
	
	@FindBy(locator ="homepage.resigter.link")
	QAFWebElement resigterLink;
	
	@FindBy(locator ="homepage.support.link")
	QAFWebElement supportLink;
	
	@FindBy(locator ="homepage.contact.link")
	QAFWebElement contactLink;
	
//	Different Section
	
	@FindBy(locator ="homepage.featureddestination.img")
	QAFWebElement featuredDestionation;
	
	@FindBy(locator ="homepage.specials.img")
	QAFWebElement specials;
	
	@FindBy(locator ="homepage.specialsbody.text")
	QAFWebElement specialsbody;
	
	@FindBy(locator ="homepage.tourtips.img")
	QAFWebElement tourTips;
	
	@FindBy(locator ="homepage.tourtipsbody.text")
	QAFWebElement tourTipsBody;
	
	@FindBy(locator ="homepage.findaflight.img")
	QAFWebElement findAFlight;
	
	@FindBy(locator ="homepage.findaflightbody.text")
	QAFWebElement findaflightbody;
	
	@FindBy(locator ="homepage.destinations.row")
	QAFWebElement destinations;
	
	@FindBy(locator ="homepage.destinationsbody.text")
	QAFWebElement destinationsbody;
	
	@FindBy(locator ="homepage.vactions.row")
	QAFWebElement vactions;
	
	@FindBy(locator ="homepage.vactionsbody.text")
	QAFWebElement vactionsbody;
	
	@FindBy(locator ="homepage.registerdifferent.row")
	QAFWebElement registerdifferent;
	
	@FindBy(locator ="homepage.registerbody.text")
	QAFWebElement registerBody;
	
	@FindBy(locator ="homepage.links.img")
	QAFWebElement links;
	
	@FindBy(locator ="homepage.linksbody.link")
	QAFWebElement linksBody;
	
//	Sign in
	
	@FindBy(locator="signin.user.txtfield")
	QAFWebElement userNameField;
	
	@FindBy(locator ="signin.password.txtfield")
	QAFWebElement passwordField;
	
	@FindBy(locator="signin.login.btn")
	QAFWebElement loginBtn;
	
	//Contact
	@FindBy(locator="contact.back.link")
	QAFWebElement backBtn;
	
	
	public QAFWebElement getHomelink()
	{
		return homelink;
	}
	
	public QAFWebElement getFlightLink() {
		return flightLink;
	}
	

	public QAFWebElement getHotelsLink() {
		return hotelsLink;
	}
	
	public QAFWebElement getCarRentalLink() {
		return carrentalLink;
	}
	
	public QAFWebElement getCruisesLink() {
		return cruisesLink;
	}
	
	public QAFWebElement getDestinationLink() {
		return destinationLink;
	}
	
	public QAFWebElement getVactionsLink() {
		return vactionsLink;
	}
	
	public QAFWebElement getSignOn() {
		return signOn;
	}
	
	
	public QAFWebElement getResigterLink() {
		return resigterLink;
	}
	
	public QAFWebElement getSupportLink() {
		return supportLink;
	}
	
	public QAFWebElement getContactLink() {
		return contactLink;
	}
	
	public QAFWebElement getFeaturedDestination() {
		return featuredDestionation;
	}
	
	public QAFWebElement getSpecials() {
		return specials;
	}
	
	public QAFWebElement getSpecialsBody() {
		return specialsbody;
	}
	
	public QAFWebElement getTourTips() {
		return tourTips;
	}
	
	public QAFWebElement getTourTipsBody() {
		return tourTipsBody;
	}
	
	public QAFWebElement getfindAFlight() {
		return findAFlight;
	}
	
	public QAFWebElement getfindAFlightBody() {
		return findaflightbody;
	}
	
	public QAFWebElement getDestinations() {
		return destinations;
		
	}
	
	public QAFWebElement getDestinationsBody() {
		return destinationsbody;
		
	}
	
	public QAFWebElement getVactions() {
		return vactions;
	}
	
	public QAFWebElement getVactionsBody() {
		return vactionsbody;
	}
	
	public QAFWebElement getRegister() {
		return registerdifferent;
	}
	
	
	public QAFWebElement getRegisterBody() {
		return registerBody;
	}
	
	public QAFWebElement getLinks() {
		return links;
	}
	
	public QAFWebElement getLinksBody() {
		return linksBody;
	}
	
	public QAFWebElement getUserName()
	{
		return userNameField;
	}
	
	public QAFWebElement getPassword()
	{
		return passwordField;
	}
	
	public QAFWebElement getLogin()
	{
		return loginBtn;
	}
	
//	Contact 
	public QAFWebElement getBackLink()
	{
		return backBtn;
	}
	
//	Home header
	public QAFWebElement getHomeHeader()
	{
		return homeheader;
	}
	
	
	@QAFTestStep(description="Top Components")
	public void topComponent()
	{
		Reporter.log("Top Components");
		getSignOn().verifyText(StringMatcher.exact("SIGN-ON"));
		getResigterLink().verifyText(StringMatcher.exact("REGISTER"));
		getSupportLink().verifyText(StringMatcher.exact("SUPPORT"));
		getContactLink().verifyText(StringMatcher.exact("CONTACT"));
		Reporter.logWithScreenShot("Screenshot for Top Components");			
	}
	
	@QAFTestStep(description="Left Components")
	public void leftComponents()
	{
		Reporter.log("Left Components");
		getHomelink().verifyText(StringMatcher.exact("Home"));		
		getFlightLink().verifyText(StringMatcher.exact("Flights"));
		getHotelsLink().verifyText(StringMatcher.exact("Hotels"));	
		getCarRentalLink().verifyText(StringMatcher.exact("Car Rentals"));		
		getCruisesLink().verifyText(StringMatcher.exact("Cruises"));		
		getDestinationLink().verifyText(StringMatcher.exact("Destinations"));
		getVactionsLink().verifyText(StringMatcher.exact("Vacations"));
		Reporter.logWithScreenShot("Screenshot for Left Components");
	}
	
	@QAFTestStep(description="Differnt Sections")
	public void differentSections()
	 {
		 Reporter.log("Differnt Sections");	
		 getFeaturedDestination().verifyVisible();
		 getSpecials().verifyVisible();
		 getTourTips().verifyVisible(); 
		 getfindAFlight().verifyVisible();	
		 getDestinations().verifyVisible();
		 getVactions().verifyVisible();
		 getRegister().verifyVisible();
		 getLinks().verifyVisible();
		 Reporter.logWithScreenShot("ScreenShot for Different Sections");
	 }
	
	public void verifySignOnPage()
	{
		getSignOn().waitForVisible();
		Reporter.logWithScreenShot("Screenshot for sign on");
	}
	
	
	@QAFTestStep(description="Sign In")
	public void signIn(String user, String Pass)
	{
		getUserName().sendKeys(user);
		getPassword().sendKeys(Pass);
		getLogin().click();
		FlightFinderPage flight= new FlightFinderPage();
		flight.waitForPageToLoad();
	}
	
	public void clickOnBack()
	{
		getBackLink().click();
	}
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
		
	}
	
	public void waitforpagetoLoad()
	{
		if(getFeaturedDestination().verifyVisible())
		{
			Reporter.log("You are on Home Page");
		}
		else
		{
			Reporter.log("You are not on Home Page");
		}

	}	

}
