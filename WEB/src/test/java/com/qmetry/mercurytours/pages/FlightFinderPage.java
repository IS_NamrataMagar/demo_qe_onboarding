package com.qmetry.mercurytours.pages;

import com.qmetry.mercurytours.beans.FlightFormDataBean;
import com.qmetry.mercurytours.tests.HomeTest;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FlightFinderPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	@FindBy(locator ="flightfinder.headerPage.text")
	QAFWebElement flightfinderHeader;
	
	@FindBy(locator ="flightfinder.passengers.drop")
	QAFWebElement passengersDrop;
	
	@FindBy(locator ="flightfinder.departingfrom.drop")
	QAFWebElement departingFromDrop;
	
	@FindBy(locator ="flightfinder.frommonth.drop")
	QAFWebElement fromMonthDrop;
	
	@FindBy(locator ="flightfinder.fromday.drop")
	QAFWebElement fromDayDrop;
	
	@FindBy(locator ="flightfinder.arrivingin.drop")
	QAFWebElement arrivingInDrop;
	
	@FindBy(locator ="flightfinder.tomonth.drop")
	QAFWebElement toMonthDrop;
	
	@FindBy(locator ="flightfinder.today.drop")
	QAFWebElement toDayDrop;
	
	@FindBy(locator ="flightfinder.economyclass.radio")
	QAFWebElement economyClassRadioBtn;
	
	@FindBy(locator ="flightfinder.businessclass.radio")
	QAFWebElement businessClassRadioBtn;
	
	@FindBy(locator ="flightfinder.firstclass.radio")
	QAFWebElement firstClassRadioBtn;
	
	@FindBy(locator ="flightfinder.airline.drop")
	QAFWebElement airlineDrop;
	
	@FindBy(locator ="flightfinder.continue.btn")
	QAFWebElement continueFlightFinderBtn;
	
	public QAFWebElement getFlightFinderHeader()
	{
		return flightfinderHeader;
	}
	
	public QAFWebElement getPassengers()
	{
		return passengersDrop;
	}
	
	public QAFWebElement getDepartingFrom()
	{
		return departingFromDrop;
	}
	
	public QAFWebElement getFromMonth()
	{
		return fromMonthDrop;
	}
	
	public QAFWebElement getFromDay()
	{
		return fromDayDrop;
	}
	
	public QAFWebElement getArrivingIn()
	{
		return arrivingInDrop;
	}
	
	public QAFWebElement getToMonth()
	{
		return toMonthDrop;
	}
	
	public QAFWebElement getToDay()
	{
		return toDayDrop;
	}
	
	public QAFWebElement getEconomyClass()
	{
		return economyClassRadioBtn;
	}
	
	public QAFWebElement getBusinessClass()
	{
		return businessClassRadioBtn;
	}
	
	public QAFWebElement getFirstClass()
	{
		return firstClassRadioBtn;
	}
	
	public QAFWebElement getAirline()
	{
		return airlineDrop;
	}
	
	public QAFWebElement getContinueFlightFinder()
	{
		return continueFlightFinderBtn;
	}
	
	
	public void homePage()
	{
		HomePage page = new HomePage();
		page.launchPage(null);
		
		HomeTest test = new HomeTest();
		test.verifyUserIsAbleToSignInFromSignInSectionOfHomePage();
	}
	
	public void clickOnContinueFlightFinder()
	{
		getContinueFlightFinder().click();
	}
	
	public void verifyflightDetails()
	{
		String passenger = ConfigurationManager.getBundle().getString("passengers");
		String departingFrom = ConfigurationManager.getBundle().getString("departingfrom");
		String arrivingIn = ConfigurationManager.getBundle().getString("arrivingIn");
		getPassengers().sendKeys(passenger);
		getDepartingFrom().sendKeys(departingFrom);
		getArrivingIn().sendKeys(arrivingIn);
	
	}
	
	public void verifyDate(String fromMonth, String fromDay,String toMonth, String toDay)
	{
		verifyflightDetails();
		getFromMonth().sendKeys(fromMonth);
		getFromDay().sendKeys(fromDay);
		getToMonth().sendKeys(toMonth);
		getToDay().sendKeys(toDay);
		
		
	}
	
	public void fillFlightData()
	{
		FlightFormDataBean bean =new FlightFormDataBean();
		bean.fillFromConfig("flight.firstdata");
		bean.fillUiElements();
	}
	
	public void sameLocation(String passenger,String depart,String arrive,String fromMonth, String fromDay,String toMonth, String toDay)
	{
		getPassengers().sendKeys(passenger);
		getDepartingFrom().sendKeys(depart);
		getArrivingIn().sendKeys(arrive);
		getFromMonth().sendKeys(fromMonth);
		getFromDay().sendKeys(fromDay);
		getToMonth().sendKeys(toMonth);
		getToDay().sendKeys(toDay);
		
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	public void waitforpagetoLoad()
	{
		if(getFlightFinderHeader().verifyVisible())
		{
			Reporter.log("You are on Flight Finder Page");
		}
		else
		{
			Reporter.log("You are not on Flight Finder Page");
		}

	}	


}
