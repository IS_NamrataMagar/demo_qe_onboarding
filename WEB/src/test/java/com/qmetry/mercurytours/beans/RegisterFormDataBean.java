package com.qmetry.mercurytours.beans;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class RegisterFormDataBean extends BaseFormDataBean {
	
	@Randomizer(length =15,type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.firstname.txt",order=1)
	private String firstname;
	
	@Randomizer(length =15,type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.lastname.txt",order=2)
	private String lastname;
	
	@Randomizer(length =15,type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.city.txt",order=6,fieldType =Type.textbox)
	private String city;
	
	@Randomizer(length =15,type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.state.txt",order=7,fieldType =Type.textbox)
	private String state;
	
	@Randomizer(length = 10,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="register.phone.txt",order=3,fieldType =Type.textbox)
	private String phone;

	@Randomizer(length =15,type= RandomizerTypes.MIXED,suffix="@gmail.com")
	@UiElement(fieldLoc="register.email.txt",order=4)
	private String email;
	
	@Randomizer(length =15,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.address.txt",order=5,fieldType =Type.textbox)
	private String address;
	
	@Randomizer(length =5,type=RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="register.postalcode.txt",order=8,fieldType =Type.textbox)
	private String postalCode;
	
	@Randomizer(length=10,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.country.drop",order=9,fieldType =Type.selectbox)
	private String country;
	
	@Randomizer(length=10,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.username.txt",order=10,fieldType =Type.textbox)
	private String username;
	
	@Randomizer(length=10,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.password.txt",order=10,fieldType =Type.textbox)
	private String passwordreg;
	
}
