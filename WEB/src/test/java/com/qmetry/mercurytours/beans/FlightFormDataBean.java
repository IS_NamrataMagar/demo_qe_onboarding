package com.qmetry.mercurytours.beans;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.util.Randomizer;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;

public class FlightFormDataBean extends BaseFormDataBean  {

	@Randomizer()
	@UiElement(fieldLoc="flightfinder.roundtrip.radio",order=1,fieldType =Type.optionbox)
	private String roundTrip;
	
	@Randomizer(length = 10,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="flightfinder.passengers.drop",order=2,fieldType=Type.selectbox)
	private String passengers;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.departingfrom.drop",order=3,fieldType=Type.selectbox)
	private String departingFrom;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.frommonth.drop",order=4,fieldType=Type.selectbox)
	private String fromMonth;
	
	@Randomizer(length = 2,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="flightfinder.fromday.drop",order=5,fieldType=Type.selectbox)
	private String fromDay;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.arrivingin.drop",order=6,fieldType=Type.selectbox)
	private String arrivingIn;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.tomonth.drop",order=7,fieldType=Type.selectbox)
	private String toMonth;
	
	@Randomizer(length = 2,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="flightfinder.today.drop",order=7,fieldType=Type.selectbox)
	private String toDay;
	
	
}
