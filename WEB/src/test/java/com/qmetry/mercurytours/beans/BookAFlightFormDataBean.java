package com.qmetry.mercurytours.beans;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.util.Randomizer;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;

public class BookAFlightFormDataBean extends BaseFormDataBean {

	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.firstname.text",order=1)
	private String firstname;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.lastname.text",order=2)
	private String lastname;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.meal.select",order=3,fieldType=Type.selectbox)
	private String meal;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.cardtype.select",order=4,fieldType=Type.selectbox)
	private String cardtype;
	
	@Randomizer(length = 16,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="bookaflight.cardnumber.text",order=5)
	private String cardnumber;
	
	@Randomizer(length = 2,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="bookaflight.expmonth.select",order=6,fieldType=Type.selectbox)
	private String expmonth;
	
	@Randomizer(length = 4,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="bookaflight.expyear.select",order=7,fieldType=Type.selectbox)
	private String expyear;
	
	@Randomizer(length = 15,type= RandomizerTypes.MIXED)
	@UiElement(fieldLoc="bookaflight.addressbill.text",order=8)
	private String addressbill;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.citybill.text",order=9)
	private String citybill;
	
	@Randomizer(length = 12,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.statebill.text",order=10)
	private String statebill;
	
	@Randomizer(length = 10,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="bookaflight.pincodebill.text",order=11)
	private String pincodebill;
	
	@Randomizer(length = 10,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.countrybill.select",order=12,fieldType=Type.selectbox)
	private String countrybill;
	
	@Randomizer(length = 15,type= RandomizerTypes.MIXED)
	@UiElement(fieldLoc="bookaflight.addressdel.text",order=13)
	private String addressdel;
	
	@Randomizer(length = 15,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.citydel.text",order=14)
	private String citydel;
	
	@Randomizer(length = 12,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.statedel.text",order=15)
	private String statedel;
	
	@Randomizer(length = 10,type= RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="bookaflight.pincodedel.text",order=16)
	private String pincodedel;
	
	@Randomizer(length = 10,type= RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="bookaflight.countrydel.select",order=17,fieldType=Type.selectbox)
	private String countrydel;
}
