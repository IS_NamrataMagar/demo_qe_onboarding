package com.qmetry.mercurytours.components;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SelectFlightComponent extends QAFWebComponent{

	protected SelectFlightComponent(String locator) {
		super(locator);
	}
	
	 @FindBy(locator = "SelectFlight.selectradio.radiobutton")   
	 private QAFWebElement radiobutton;        
	 
	 @FindBy(locator = "SelectFlightPage.flighttitle.txt")   
	 private QAFWebElement selecttitle;

     public QAFWebElement getRadiobutton() 
     {       
    	 return radiobutton;     
     }

     public QAFWebElement getSelecttitle() 
     {       
    	 return selecttitle;   
     }
}
