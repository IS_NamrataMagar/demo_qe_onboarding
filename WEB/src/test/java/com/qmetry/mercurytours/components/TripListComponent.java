package com.qmetry.mercurytours.components;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class TripListComponent extends QAFWebComponent {

	protected TripListComponent(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(locator="flightfinder.trip.radio")
	private QAFWebElement tripType;
	
	
	public QAFWebElement getTripType()
	{
		return tripType;
	}
	

}
