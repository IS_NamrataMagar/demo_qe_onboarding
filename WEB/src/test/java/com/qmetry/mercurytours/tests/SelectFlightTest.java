package com.qmetry.mercurytours.tests;

import org.testng.annotations.Test;
import com.qmetry.mercurytours.pages.SelectFlightPage;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class SelectFlightTest extends WebDriverTestCase {

	
	@Test
	public void clickOnDepartReturnRadio(String firstflight,String secondFight)
	{
		SelectFlightPage page = new SelectFlightPage();
		page.clickOnDepart(firstflight);
        page.clickOnDepart(secondFight);
        Reporter.logWithScreenShot("Selected flights");
        page.clickOnSelectFlight();
	}
}
