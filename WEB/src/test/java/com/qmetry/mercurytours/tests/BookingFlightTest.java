package com.qmetry.mercurytours.tests;
import java.util.Map;

import org.testng.annotations.Test;
import com.qmetry.mercurytours.pages.BookingFlightPage;
import com.qmetry.mercurytours.pages.FlightConfirmationPage;
import com.qmetry.mercurytours.pages.FlightFinderPage;
import com.qmetry.mercurytours.pages.SelectFlightPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
public class BookingFlightTest extends WebDriverTestCase {

	@Test
	public void alertPopup()
	{
		BookingFlightPage bookflight =  new BookingFlightPage();
		bookflight.verifyPassengerDetails();
		getDriver().switchTo().alert().accept();
	}
	
	@Test
	@QAFDataProvider(key="passenger.name")
	public void verifyFirstLastNameValidation(Map<String,String> data)
	{
		FlightFinderPage flight = new FlightFinderPage();
		flight.launchPage(null);
		HomeTest page = new HomeTest();
		page.verifyUserIsAbleToSignInFromSignInSectionOfHomePage();
		flight.waitForPageToLoad();
		verifyTrue(flight.getFlightFinderHeader().verifyVisible(),"You are not on Flight Finder Page","You are on Flight Finder Page");
		flight.fillFlightData();
		flight.clickOnContinueFlightFinder();
		flight.waitForPageToLoad();
		SelectFlightPage selectflight = new SelectFlightPage();
		selectflight.waitForPageToLoad();
		verifyTrue(selectflight.getSelectflighttitle().verifyVisible(),"You are not on Select Flight Page","You are on Select Flight Page");
		selectflight.clickOnDepart(String.valueOf(data.get("firstflight")));
		selectflight.clickOnDepart(String.valueOf(data.get("secondflight")));
		selectflight.clickOnSelectFlight();

		BookingFlightPage bookflight =  new BookingFlightPage();
		bookflight.waitForPageToLoad();
		verifyTrue(bookflight.getBookaFlightHeader().verifyVisible(),"You are not on Book A Flight Page","You are on Book A Flight Page");
		verifyTrue(bookflight.getsummary().verifyVisible(),"Summary is not verified","Summary is Verified");
		bookflight.validationOnName(String.valueOf(data.get("firstname")), String.valueOf(data.get("lastname")));
		bookflight.clickOnSecurePurchase();
		
		FlightConfirmationPage confirmation = new FlightConfirmationPage();
		confirmation.waitForPageToLoad();
		verifyTrue(confirmation.getConfirmationText().verifyVisible(),"You are not on Flight Confirmation Page","You are on Flight Confirmation Page");
		verifyTrue(confirmation.getConfirmationSummary().verifyVisible(),"Confiramtion Page Text is not Verified","Confiramtion Page Text is  Verified");
	}
	
	
	@Test
	@QAFDataProvider(key="passenger.card")
	public void verifyCardValidation(Map<String,String> data)
	{
		FlightFinderPage flight = new FlightFinderPage();
		flight.launchPage(null);
		HomeTest page = new HomeTest();
		page.verifyUserIsAbleToSignInFromSignInSectionOfHomePage();
		flight.waitForPageToLoad();
		verifyTrue(flight.getFlightFinderHeader().verifyVisible(),"You are not on Flight Finder Page","You are on Flight Finder Page");
		flight.fillFlightData();
		flight.clickOnContinueFlightFinder();
		flight.waitForPageToLoad();
		SelectFlightPage selectflight = new SelectFlightPage();
		selectflight.waitForPageToLoad();
		verifyTrue(selectflight.getSelectflighttitle().verifyVisible(),"You are not on Select Flight Page","You are on Select Flight Page");
		selectflight.clickOnDepart(String.valueOf(data.get("firstflight")));
		selectflight.clickOnDepart(String.valueOf(data.get("secondflight")));
		selectflight.clickOnSelectFlight();

		BookingFlightPage bookflight =  new BookingFlightPage();
		bookflight.waitForPageToLoad();
		verifyTrue(bookflight.getBookaFlightHeader().verifyVisible(),"You are not on Book A Flight Page","You are on Book A Flight Page");
		verifyTrue(bookflight.getsummary().verifyVisible(),"Summary is not verified","Summary is Verified");
		bookflight.validationOnCard(String.valueOf(data.get("cardtype")),String.valueOf(data.get("cardnumber")));
		bookflight.clickOnSecurePurchase();
		
		FlightConfirmationPage confirmation = new FlightConfirmationPage();
		confirmation.waitForPageToLoad();
		verifyTrue(confirmation.getConfirmationText().verifyVisible(),"You are not on Flight Confirmation Page","You are on Flight Confirmation Page");
		verifyTrue(confirmation.getConfirmationSummary().verifyVisible(),"Confiramtion Page Text is not Verified","Confiramtion Page Text is  Verified");
	}
	
	@Test
	@QAFDataProvider(key="passenger.expiry")
	public void verifyCardExpiryValidation(Map<String,String> data)
	{
		FlightFinderPage flight = new FlightFinderPage();
		flight.launchPage(null);
		HomeTest page = new HomeTest();
		page.verifyUserIsAbleToSignInFromSignInSectionOfHomePage();
		flight.waitForPageToLoad();
		verifyTrue(flight.getFlightFinderHeader().verifyVisible(),"You are not on Flight Finder Page","You are on Flight Finder Page");
		flight.fillFlightData();
		flight.clickOnContinueFlightFinder();
		flight.waitForPageToLoad();
		SelectFlightPage selectflight = new SelectFlightPage();
		selectflight.waitForPageToLoad();
		verifyTrue(selectflight.getSelectflighttitle().verifyVisible(),"You are not on Select Flight Page","You are on Select Flight Page");
		selectflight.clickOnDepart(String.valueOf(data.get("firstflight")));
		selectflight.clickOnDepart(String.valueOf(data.get("secondflight")));
		selectflight.clickOnSelectFlight();

		BookingFlightPage bookflight =  new BookingFlightPage();
		bookflight.waitForPageToLoad();
		verifyTrue(bookflight.getBookaFlightHeader().verifyVisible(),"You are not on Book A Flight Page","You are on Book A Flight Page");
		verifyTrue(bookflight.getsummary().verifyVisible(),"Summary is not verified","Summary is Verified");
		bookflight.validationOnExpiration(String.valueOf(data.get("cardtype")),String.valueOf(data.get("cardnumber")),String.valueOf(data.get("expmonth")),String.valueOf(data.get("expyear")));
		bookflight.clickOnSecurePurchase();
		
		FlightConfirmationPage confirmation = new FlightConfirmationPage();
		confirmation.waitForPageToLoad();
		verifyTrue(confirmation.getConfirmationText().verifyVisible(),"You are not on Flight Confirmation Page","You are on Flight Confirmation Page");
		verifyTrue(confirmation.getConfirmationSummary().verifyVisible(),"Confiramtion Page Text is not Verified","Confiramtion Page Text is  Verified");
	}
	
	
}
