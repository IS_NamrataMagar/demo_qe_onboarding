package com.qmetry.mercurytours.tests;

import java.util.Map;
import org.testng.annotations.Test;
import com.qmetry.mercurytours.pages.BookingFlightPage;
import com.qmetry.mercurytours.pages.FlightConfirmationPage;
import com.qmetry.mercurytours.pages.FlightFinderPage;
import com.qmetry.mercurytours.pages.SelectFlightPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class FlightFinderTest extends WebDriverTestCase {
	
	@Test
	@QAFDataProvider(key="dateValidation.date")
	public void verifyDate(Map<String,String> data)
	{

		FlightFinderPage flight = new FlightFinderPage();
		flight.launchPage(null);
		HomeTest page = new HomeTest();
		page.verifyUserIsAbleToSignInFromSignInSectionOfHomePage();
		flight.waitForPageToLoad();
		verifyTrue(flight.getFlightFinderHeader().verifyVisible(),"You are not on Flight Finder Page","You are on Flight Finder Page");
		flight.verifyDate(String.valueOf(data.get("fromMonth")),String.valueOf(data.get("fromDay")),String.valueOf(data.get("toMonth")),String.valueOf(data.get("toDay")));
		flight.clickOnContinueFlightFinder();
		flight.waitForPageToLoad();
		SelectFlightPage selectflight = new SelectFlightPage();
		selectflight.waitForPageToLoad();
		verifyTrue(selectflight.getSelectflighttitle().verifyVisible(),"You are not on Select Flight Page","You are on Select Flight Page");
		selectflight.clickOnDepart(String.valueOf(data.get("firstflight")));
		selectflight.clickOnDepart(String.valueOf(data.get("secondflight")));
		selectflight.clickOnSelectFlight();
		
		BookingFlightPage bookflight =  new BookingFlightPage();
		bookflight.waitForPageToLoad();
		verifyTrue(bookflight.getBookaFlightHeader().verifyVisible(),"You are not on Book A Flight Page","You are on Book A Flight Page");
		verifyTrue(bookflight.getsummary().verifyVisible(),"Summary is not verified","Summary is Verified");
	}
	
	@Test
	@QAFDataProvider(key="flight.seconddata")
	public void verifySameLocation(Map<String,String> data)
	{
		FlightFinderPage flight = new FlightFinderPage();
		flight.launchPage(null);
		HomeTest page = new HomeTest();
		page.verifyUserIsAbleToSignInFromSignInSectionOfHomePage();
		flight.waitForPageToLoad();
		verifyTrue(flight.getFlightFinderHeader().verifyVisible(),"You are not on Flight Finder Page","You are on Flight Finder Page");
		flight.sameLocation(String.valueOf(data.get("passengers")), String.valueOf(data.get("departingFrom")),String.valueOf(data.get("arrivingIn")), String.valueOf(data.get("fromMonth")),String.valueOf(data.get("fromDay")),String.valueOf(data.get("toMonth")),String.valueOf(data.get("toDay")));
		flight.clickOnContinueFlightFinder();
		flight.waitForPageToLoad();
		SelectFlightPage selectflight = new SelectFlightPage();
		selectflight.waitForPageToLoad();
		verifyTrue(selectflight.getSelectflighttitle().verifyVisible(),"You are not on Select Flight Page","You are on Select Flight Page");
		selectflight.clickOnDepart(String.valueOf(data.get("firstflight")));
		selectflight.clickOnDepart(String.valueOf(data.get("secondflight")));
		selectflight.clickOnSelectFlight();
		
		BookingFlightPage bookflight =  new BookingFlightPage();
		bookflight.waitForPageToLoad();
		verifyTrue(bookflight.getBookaFlightHeader().verifyVisible(),"You are not on Book A Flight Page","You are on Book A Flight Page");
		verifyTrue(bookflight.getsummary().verifyVisible(),"Summary is not verified","Summary is Verified");
	}
	
	@Test
	@QAFDataProvider(key = "bookflight.flight.data")
	public void verifyFlightFinderDetails(Map<String,String> data)
	{
		
		FlightFinderPage flight = new FlightFinderPage();
		flight.launchPage(null);
		HomeTest page = new HomeTest();
		page.verifyUserIsAbleToSignInFromSignInSectionOfHomePage();
		flight.waitForPageToLoad();
		verifyTrue(flight.getFlightFinderHeader().verifyVisible(),"You are not on Flight Finder Page","You are on Flight Finder Page");
		flight.fillFlightData();
		flight.clickOnContinueFlightFinder();
		flight.waitForPageToLoad();
		SelectFlightPage selectflight = new SelectFlightPage();
		selectflight.waitForPageToLoad();
		verifyTrue(selectflight.getSelectflighttitle().verifyVisible(),"You are not on Select Flight Page","You are on Select Flight Page");
		selectflight.clickOnDepart(String.valueOf(data.get("firstflight")));
		selectflight.clickOnDepart(String.valueOf(data.get("secondflight")));
		selectflight.clickOnSelectFlight();

		BookingFlightPage bookflight =  new BookingFlightPage();
		bookflight.waitForPageToLoad();
		verifyTrue(bookflight.getBookaFlightHeader().verifyVisible(),"You are not on Book A Flight Page","You are on Book A Flight Page");
		verifyTrue(bookflight.getsummary().verifyVisible(),"Summary is not verified","Summary is Verified");
		BookingFlightTest booktest= new BookingFlightTest();
		booktest.alertPopup();
		Reporter.logWithScreenShot("Passengers deatils are filled");
		bookflight.clickOnSecurePurchase();
		
		FlightConfirmationPage confirmation = new FlightConfirmationPage();
		confirmation.waitForPageToLoad();
		verifyTrue(confirmation.getConfirmationText().verifyVisible(),"You are not on Flight Confirmation Page","You are on Flight Confirmation Page");
		verifyTrue(confirmation.getConfirmationSummary().verifyPresent(),"Confiramtion Page Text is not Verified","Confiramtion Page Text is  Verified");
		confirmation.clickOnBackBtn();
		flight.waitForPageToLoad();
		verifyTrue(flight.getFlightFinderHeader().verifyVisible(),"You are not on Flight Finder Page","You are on Flight Finder Page");		
	}
}

