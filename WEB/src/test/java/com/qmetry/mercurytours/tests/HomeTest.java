package com.qmetry.mercurytours.tests;

import java.nio.file.FileSystemNotFoundException;
import java.util.Map;

import org.testng.annotations.Test;

import com.qmetry.mercurytours.pages.BookingFlightPage;
import com.qmetry.mercurytours.pages.ContactPage;
import com.qmetry.mercurytours.pages.FlightConfirmationPage;
import com.qmetry.mercurytours.pages.FlightFinderPage;
import com.qmetry.mercurytours.pages.HomePage;
import com.qmetry.mercurytours.pages.SelectFlightPage;
import com.qmetry.mercurytours.pages.SignOnPage;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class HomeTest extends WebDriverTestCase {
	@Test
	public void verifyDifferentComponentsAndLinksOfTheHomepage()
	{
		HomePage home = new HomePage();
		home.launchPage(null);
		home.topComponent();	
		home.leftComponents();
		home.differentSections();
		verifyTrue(home.getSpecialsBody().verifyVisible(),"Text is not verified from Specials body section","Text is verified from Specials body section");
		verifyTrue(home.getTourTipsBody().verifyVisible(),"Text are not verified from Tour Tips Body section","Text are verified from Tour Tips Body section");
		verifyTrue(home.getfindAFlightBody().verifyVisible(),"'sign-in here' text is not verified from Find A Flight Body section","'sign-in here' text is verified from Find A Flight Body section");
		verifyTrue(home.getDestinationsBody().verifyVisible(),"'Find detailed information about'text is not veriffied from Destination body Section","'Find detailed information about'text is veriffied from Destination body Section");
		verifyTrue(home.getRegisterBody().verifyVisible(),"'Register here' text is not verified from Register Body section","'Register here' text is verified from Register Body section");
		verifyTrue(home.getLinksBody().verifyVisible(),"'Business Travel@about.com' text is not verified from Links Body Section","'Business Travel@about.com' text is verified from Links Body Section");
	}
	
	@Test
	public void verifyUserIsAbleToSignInFromSignInSectionOfHomePage()
	{
		
	HomePage home = new HomePage();
	home.launchPage(null);
	String userName = ConfigurationManager.getBundle().getString("username");
	String password = ConfigurationManager.getBundle().getString("password");
	home.signIn(userName,password);
	FlightFinderPage ff = new FlightFinderPage();
	ff.waitForPageToLoad();
	verifyTrue(ff.getFlightFinderHeader().verifyVisible(), "User is not able to login", "User is able to Login");
	}


	@Test
	public void verifyLogout()
	{
		HomePage home = new HomePage();
		home.launchPage(null);
		String userName = ConfigurationManager.getBundle().getString("username");
		String password = ConfigurationManager.getBundle().getString("password");
		home.signIn(userName,password);
		
		FlightFinderPage ff = new FlightFinderPage();
		ff.waitForPageToLoad();
		ff.clickOnContinueFlightFinder();
		SelectFlightPage sfp= new SelectFlightPage();
		sfp.waitForPageToLoad();
		sfp.clickOnSelectFlight();
		BookingFlightPage bfg = new BookingFlightPage();
		bfg.waitForPageToLoad();
		bfg.clickOnSecurePurchase();
		FlightConfirmationPage fcp = new FlightConfirmationPage();
		fcp.clickOnLogout();
		SignOnPage home1 = new SignOnPage();
		home1.waitForPageToLoad();
		verifyTrue(home1.getSignOnHeader().verifyVisible(), "User is not able to logout or not navigating to the home page", "User is able to logout and navigated to SignOn page");
	}
	
	@QAFDataProvider(key ="login.data")
	@Test(description="XML Data")
	public void invalidCredentials(Map<String,String> data) throws FileSystemNotFoundException
	{
		HomePage home = new HomePage();
		home.launchPage(null);
		home.getUserName().sendKeys(data.get("username"));
		home.getPassword().sendKeys(data.get("password"));
		home.getLogin().click();
		
		SignOnPage signpage =new SignOnPage();
		signpage.waitForPageToLoad();
		verifyTrue(signpage.getSignOnHeader().verifyVisible(),"Vaild Credentials ,You are on Flight finder page ","Invalid Credentials,You are on Sign on page");
	}

	@Test
	public void backToHomePageFromContactPage()
	{
		HomePage home = new HomePage();
		home.launchPage(null);
		home.getContactLink().click();
		
		
		ContactPage contact = new ContactPage();
		System.out.println("Header="+contact.getText());
		contact.waitforpagetoLoad();
		home.clickOnBack();
		home.waitForPageToLoad();
		Reporter.logWithScreenShot("Back to home page");
		
	}
}
