package com.qmetry.mercurytours.tests;

import org.testng.annotations.Test;

import java.util.Map;
import com.qmetry.mercurytours.pages.RegisterPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class RegisterTest extends WebDriverTestCase {

	@Test
	public void verifyRegisterUser()
	{
		RegisterPage register = new RegisterPage();
		register.launchPage(null);
		register.registerUser();
		register.waitforpagetoLoad();
		verifyTrue(register.getRegisterMessage().verifyVisible(),"You will NOT get 'Thankyou you are registered' message","You will get 'Thankyou you are registered' meassge!!");       
	}
	
	@QAFDataProvider(dataFile="resources/TestData/InvalidUserforRegister.xls",sheetName="XLSData",key="XLSDataSP")
	@Test
	public void verifyInvalidRegisterUser(Map<String,String> data)
	{
		RegisterPage register = new RegisterPage();
		register.launchPage(null);
		register.invalidRegisterUser(String.valueOf(data.get("Firstname")),String.valueOf(data.get("Lastname")),String.valueOf(data.get("Phone")),String.valueOf(data.get("Email")));
		register.waitforpagetoLoad();
		verifyTrue(register.getRegisterMessage().verifyVisible(),"You will NOT get 'Thankyou you are registered' message","You will get 'Thankyou you are registered' meassge!!");       
	}
}
