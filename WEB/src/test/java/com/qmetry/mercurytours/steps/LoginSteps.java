package com.qmetry.mercurytours.steps;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class LoginSteps {

	@QAFTestStep(description="User open new tour application")
	public void openApp()
	{
		CommonStep.get("/");
	}
	
	@QAFTestStep(description="user login with valid {0} and valid {1}")
	public void doLogin(String user,String pass)
	{
		String userName = ConfigurationManager.getBundle().getString("username");
		String password = ConfigurationManager.getBundle().getString("password");
		CommonStep.sendKeys(userName, "signin.user.txtfield");
		CommonStep.sendKeys(password, "signin.password.txtfield");
		CommonStep.click("signin.login.btn");
	}
	
	@QAFTestStep(description="verify user loggedIn successful")
	public void verifySuccess()
	{
		System.out.println("Login Successful");
	}
}
