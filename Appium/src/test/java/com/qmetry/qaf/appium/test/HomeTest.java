package com.qmetry.qaf.appium.test;

import java.util.Map;

import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import com.qmetry.qaf.appium.pages.HomePage;
import com.qmetry.qaf.appium.pages.LandingPage;
import com.qmetry.qaf.appium.pages.LoginPage;
import com.qmetry.qaf.appium.pages.MenuPage;
import com.qmetry.qaf.appium.utility.Scroll;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class HomeTest extends WebDriverTestCase {

	@QAFDataProvider(key = "user1.data")
	@Test
	public void verifyThatUserCanRegisterHimselfByProvidingCorrectDetails(Map<String, String> data) {
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnSkipSignIn();

		HomePage home = new HomePage();
		home.waitForPageToLoad();

		MenuPage menu = new MenuPage();
		menu.waitForPageToLoad();
		verifyTrue(menu.getmenuSignInText().verifyPresent(),
				"You are not on Home of Amazon Page", "You are on Home of Amazon Page");

		menu.clickOntabMenu();
		verifyTrue(menu.getLeftMenuHeader().verifyPresent(),
				"You are not on Left Menu bar", "You are on Left menu Bar");
		menu.clickonSignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.clickOnCreateAccount();

		home.waitForPageToLoad();
		login.fillDetailsForCreateAccount();
		getDriver().switchTo().alert().accept();
		Reporter.logWithScreenShot("Data are filled");
		Scroll sc = new Scroll();
		sc.scrollVertical();
		login.clickOnVerifyMobileBtn();
		login.waitForPageToLoad();
		verifyTrue(login.getMobileVerifyHeader().verifyPresent(),
				"You are not on Mobile Verification Page", "You are on Mobile Verification Page");
	}

	@QAFDataProvider(key = "user2.data")
	@Test
	public void verifyRegisteringWithExistingUser(Map<String, String> data) {
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnSkipSignIn();

		HomePage home = new HomePage();
		home.waitForPageToLoad();

		MenuPage menu = new MenuPage();
		menu.waitForPageToLoad();
		verifyTrue(menu.getmenuSignInText().verifyPresent(),
				"You are not on Home of Amazon Page", "You are on Home of Amazon Page");

		menu.clickOntabMenu();
		verifyTrue(menu.getLeftMenuHeader().verifyPresent(),
				"You are not on Left Menu bar", "You are on Left menu Bar");
		menu.clickonSignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.clickOnCreateAccount();

		home.waitForPageToLoad();
		login.fillDetailsForCreateAccount();

		Reporter.logWithScreenShot("Data are filled");
		Scroll sc = new Scroll();
		sc.scrollVertical();
		login.clickOnVerifyMobileBtn();
		verifyTrue(login.getVerifyRegisterUserMsg().verifyPresent(),
				"You are not a Registered user", "You are registerd user");
	}

	@QAFDataProvider(key = "user3.data")
	@Test
	public void verifyVerifyExistingUserCanLogin(Map<String, String> data) {
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnAlreadySignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		login.getEmailMobileNumber().sendKeys(String.valueOf(data.get("mobileemail")));
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.getContinueBtn().click();
		login.waitForPageToLoad();
		verifyTrue(login.getHeaderLogin().verifyPresent(),
				"You are not on Login Page to put password", "You are on Login page to put password");
		login.getPassword().sendKeys(data.get("pass"));
		login.getLoginSubmit().click();

		HomePage home = new HomePage();
		home.waitForPageToLoad();
		verifyTrue(home.getHomeHeader().verifyPresent(), "You are not on Home Page",
				"You are on Home Page");

	}

	@QAFDataProvider(key = "user3.data")
	@Test
	public void verifyThatUserIsAbleToNavigateThroughAllTheProductsAcrossDifferentCategories(Map<String, String> data) {
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnAlreadySignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		login.getEmailMobileNumber().sendKeys(String.valueOf(data.get("mobileemail")));
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.getContinueBtn().click();
		login.waitForPageToLoad();
		verifyTrue(login.getHeaderLogin().verifyPresent(),
				"You are not on Login Page to put password", "You are on Login page to put password");
		login.getPassword().sendKeys(data.get("pass"));
		login.getLoginSubmit().click();

		HomePage home = new HomePage();
		home.waitForPageToLoad();
		verifyTrue(home.getHomeHeader().verifyPresent(), "You are not on Home Page",
				"You are on Home Page");
		
		MenuPage menu = new MenuPage();
		menu.waitForPageToLoad();
		verifyTrue(menu.getmenuSignInText().verifyPresent(),
				"You are not on Home of Amazon Page", "You are on Home of Amazon Page");
		menu.getShopByCategory().click();
		verifyTrue(menu.getMensFashionLink().verifyPresent(),"Yo are not clicked Men's Link","You Clicked Men's Link");
		menu.getMensFashionLink().click();
		verifyTrue(menu.getAmazonMensFashionLink().verifyPresent(),"Yo are not clicked Men's Link","You Clicked Men's Link");
		menu.getAmazonMensFashionLink().click();
		menu.getMenLink().click();
		menu.getClickOnAllProduct().click();
		verifyTrue(menu.getSock().verifyPresent(),"Yo are not clicked Sock's Link","You Clicked Sock's Link");
		menu.getSock().click();
		
	}
	
	@QAFDataProvider(key = "user3.data")
	@Test
	public void verifyThatOnSearchingAllTheProductSatisfyingTheSearchCriteriaAreVisibleOnTheSearchResultPage(Map<String, String> data)
	{
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnAlreadySignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		login.getEmailMobileNumber().sendKeys(String.valueOf(data.get("mobileemail")));
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.getContinueBtn().click();
		login.waitForPageToLoad();
		verifyTrue(login.getHeaderLogin().verifyPresent(),
				"You are not on Login Page to put password", "You are on Login page to put password");
		login.getPassword().sendKeys(data.get("pass"));
		login.getLoginSubmit().click();

		HomePage home = new HomePage();
		home.waitForPageToLoad();
		verifyTrue(home.getHomeHeader().verifyPresent(), "You are not on Home Page",
				"You are on Home Page");
		
		home.getSearchBox().sendKeys("shoe men sports\n");
		home.waitForPageToLoad();
//		home.getSearchBox().sendKeys(Keys.ENTER);
		verifyTrue(home.getSearchShoe().verifyPresent(),"You have not selected shoe","You are selected the shoe");
		home.getSearchShoe().click();
		
		
	}
	
	@QAFDataProvider(key = "user3.data")
	@Test
	public void verifyTheMoreRelevantProductForTheSearchTermAreDisplayedOnTheTopForAParticularSearchTerm(Map<String, String> data)
	{
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnAlreadySignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		login.getEmailMobileNumber().sendKeys(String.valueOf(data.get("mobileemail")));
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.getContinueBtn().click();
		login.waitForPageToLoad();
		verifyTrue(login.getHeaderLogin().verifyPresent(),
				"You are not on Login Page to put password", "You are on Login page to put password");
		login.getPassword().sendKeys(data.get("pass"));
		login.getLoginSubmit().click();

		HomePage home = new HomePage();
		home.waitForPageToLoad();
		verifyTrue(home.getHomeHeader().verifyPresent(), "You are not on Home Page",
				"You are on Home Page");
		
		home.getSearchBox().sendKeys("Women's shoes\n");
		home.waitForPageToLoad();
		String number =home.getSearchResult().getText();
		 number=number.replaceAll("[^0-9]", "");
		 Reporter.log("Total result found for Women's Shoe are"+number);
		home.getWomenShoe().click();
		home.waitForPageToLoad();
		verifyTrue(home.getWomenShoeHeader().verifyPresent(),"You have not selectes women shoe","You selected women shoe");
		
	}
	
	@QAFDataProvider(key = "user3.data")
	@Test
	public void verifyThatFilteringWorksCorrectlyOnTheSearchResultPage(Map<String, String> data)
	{
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnAlreadySignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		login.getEmailMobileNumber().sendKeys(String.valueOf(data.get("mobileemail")));
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.getContinueBtn().click();
		login.waitForPageToLoad();
		verifyTrue(login.getHeaderLogin().verifyPresent(),
				"You are not on Login Page to put password", "You are on Login page to put password");
		login.getPassword().sendKeys(data.get("pass"));
		login.getLoginSubmit().click();

		HomePage home = new HomePage();
		home.waitForPageToLoad();
		verifyTrue(home.getHomeHeader().verifyPresent(), "You are not on Home Page",
				"You are on Home Page");
		
		home.getSearchBox().sendKeys("Books\n");
		home.waitForPageToLoad();
		
		home.getFilter().click();
		verifyTrue(home.getFilter().verifyPresent(),"You have not selected Filter","You have selected Filter");
		home.getFormat().click();
		verifyTrue(home.getFormat().verifyPresent(),"Format Link is not present","Format link is Present");
		home.getPapaerBack().click();
		verifyTrue(home.getPapaerBack().verifyText("Paperback"),"You have not selected papar back","You have selected Paperback");
		home.getFilter1().click();
		
		MenuPage menu = new MenuPage();
		menu.waitForPageToLoad();
		verifyTrue(menu.getmenuSignInText().verifyPresent(),
				"You are not on Home of Amazon Page", "You are on Home of Amazon Page");
		home.waitForPageToLoad();
		String number =home.getSearchResult().getText();
		 number=number.replaceAll("[^0-9]", "");
		 Reporter.log("Total result found for Books are "+number);
	}
	
	@QAFDataProvider(key = "user3.data")
	@Test
	public void verifyThatUserCanAddToCartOneOrMoreProducts(Map<String, String> data)
	{
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnAlreadySignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		login.getEmailMobileNumber().sendKeys(String.valueOf(data.get("mobileemail")));
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.getContinueBtn().click();
		login.waitForPageToLoad();
		verifyTrue(login.getHeaderLogin().verifyPresent(),
				"You are not on Login Page to put password", "You are on Login page to put password");
		login.getPassword().sendKeys(data.get("pass"));
		login.getLoginSubmit().click();

		HomePage home = new HomePage();
		home.waitForPageToLoad();
		verifyTrue(home.getHomeHeader().verifyPresent(), "You are not on Home Page",
				"You are on Home Page");
		
		home.getSearchBox().sendKeys("Harpa Tops\n");
		home.waitForPageToLoad();
		Scroll sc = new Scroll();
		sc.scrollVertical();
		home.getHarpaTop().click();
		home.waitForPageToLoad();
		sc.scrollVertical();
		verifyTrue(home.getHarpaTop().verifyPresent(), "You are not on Harpa top page", "you are on harpa top page");
		home.getTopSize().click();
		sc.scrollVertical();
		sc.scrollVertical();
		home.getAddToCart().click();
		home.waitForPageToLoad();

		home.getViewCartTop().click();
		home.waitForPageToLoad();
		verifyTrue(home.getCartPage().verifyPresent(),"You are not on Cart","You are on Cart");
		home.getSearchProduct().clear();
		home.getSearchProduct().sendKeys("Lipsticks\n");
	}
	
	@QAFDataProvider(key = "user3.data")
	@Test
	public void VerifyThatSortingWorksCorrectlyOnTheSearchResultPage(Map<String, String> data)
	{
		LandingPage landing = new LandingPage();
		landing.waitForPageToLoad();
		verifyTrue(landing.getLandingHeader().verifyPresent(),
				"You are not on Amazon Landing Page", "You are on Amazon Landing Page");
		landing.clickOnAlreadySignIn();

		LoginPage login = new LoginPage();
		login.waitForPageToLoad();
		login.getEmailMobileNumber().sendKeys(String.valueOf(data.get("mobileemail")));
		verifyTrue(login.getLoginHeader().verifyPresent(),
				"You are not on Login /Create Account Page", "You are on Login/Create Account Page");
		login.getContinueBtn().click();
		login.waitForPageToLoad();
		verifyTrue(login.getHeaderLogin().verifyPresent(),
				"You are not on Login Page to put password", "You are on Login page to put password");
		login.getPassword().sendKeys(data.get("pass"));
		login.getLoginSubmit().click();

		HomePage home = new HomePage();
		home.waitForPageToLoad();
		verifyTrue(home.getHomeHeader().verifyPresent(), "You are not on Home Page",
				"You are on Home Page");
		
		home.getSearchBox().sendKeys("Books\n");
		home.waitForPageToLoad();
		
		home.getFilter().click();
		verifyTrue(home.getFilter().verifyPresent(),"You have not selected Filter","You have selected Filter");
		home.getSort().click();
		verifyTrue(home.getSort().verifyPresent(),"Sort Link is not present","Sort link is Present");
		
		home.getPriceLowToHigh().click();
		verifyTrue(home.getPriceLowToHigh().verifyText("Price: Low to High"),"You have not selected Price: Low to high","You are selected Price: Low to High");
		home.getFilter1().click();
		
		MenuPage menu = new MenuPage();
		menu.waitForPageToLoad();
		verifyTrue(menu.getmenuSignInText().verifyText(menu.getmenuSignInText().getText()),
				"You are not on Home of Amazon Page", "You are on Home of Amazon Page");
		home.waitForPageToLoad();
	}
}
