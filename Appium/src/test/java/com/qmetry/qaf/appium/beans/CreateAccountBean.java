package com.qmetry.qaf.appium.beans;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class CreateAccountBean extends BaseFormDataBean {
	
	@Randomizer(length =15,type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="login.name.text",order=1)
	private String name;
	
	@Randomizer(length =10,type = RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="login.phonenumber.text",order=2)
	private String phone;

//	@Randomizer(length =15,type= RandomizerTypes.MIXED,suffix="@gmail.com")
//	@UiElement(fieldLoc="login.emailid.text",order=3)
//	private String email;
	
	@Randomizer(length =15,type= RandomizerTypes.MIXED,suffix="@gmail.com")
	@UiElement(fieldLoc="login.password.text",order=3)
	private String password;

}
