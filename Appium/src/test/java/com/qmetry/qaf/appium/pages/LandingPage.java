package com.qmetry.qaf.appium.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class LandingPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	
	@FindBy(locator="landing.skiplogin.btn")
	QAFWebElement skipsigninbtn;
	
	@FindBy(locator="landing.alreadysignin.btn")
	QAFWebElement alreadysignin;
	
	@FindBy(locator="landing.landingheader.text")
	QAFWebElement landingheader;
	
	
	
	public QAFWebElement getSkipSignIn()
	{
		return skipsigninbtn;
	}
	
	public QAFWebElement getAlreadySignIn()
	{
		return alreadysignin;
	}
	
	public QAFWebElement getLandingHeader()
	{
		return landingheader;
	}
	
	public void clickOnSkipSignIn()
	{
		getSkipSignIn().click();
	}
	
	public void clickOnAlreadySignIn()
	{
		getAlreadySignIn().click();	
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
