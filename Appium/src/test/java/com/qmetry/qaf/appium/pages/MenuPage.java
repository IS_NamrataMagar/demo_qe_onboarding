package com.qmetry.qaf.appium.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class MenuPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="menu.tab.tab")
	QAFWebElement tabonmenu;
	
	@FindBy(locator="menu.homesignin.text")
	QAFWebElement menusignintext;
	
	@FindBy(locator="menu.leftmenuheaderhome.text")
	QAFWebElement leftmenuheader;
	
	@FindBy(locator="menu.shopbycategory.link")
	QAFWebElement shopbycategory;
	
	@FindBy(locator="menu.mensfashion.link")
	QAFWebElement mensfashionlink;
	
	@FindBy(locator="menu.amazonmensfashion.link")
	QAFWebElement amazonmensfashionlink;
	
	@FindBy(locator="menu.men.link")
	QAFWebElement menlink;
	
	@FindBy(locator="menu.clickonallproduct.link")
	QAFWebElement clickonallproduct;
	
	@FindBy(locator="menu.sock.item")
	QAFWebElement sock;
	
	public QAFWebElement getMensFashionLink()
	{
		return mensfashionlink;
	}
	public QAFWebElement getAmazonMensFashionLink()
	{
		return amazonmensfashionlink;
	}
	public QAFWebElement getMenLink()
	{
		return menlink;
	}
	public QAFWebElement getClickOnAllProduct()
	{
		return clickonallproduct;
	}
	public QAFWebElement getSock()
	{
		return sock;
	}
	
	public QAFWebElement getShopByCategory()
	{
		return shopbycategory;
	}
	public QAFWebElement getTabOnMenu()
	{
		return tabonmenu;
	}
	
	public QAFWebElement getmenuSignInText()
	{
		return menusignintext;
	}
	
	public QAFWebElement getLeftMenuHeader()
	{
		return leftmenuheader;
	}
	
	

	public void clickOntabMenu()
	{
		getTabOnMenu().click();
	}
	
	public void clickonSignIn()
	{
		getmenuSignInText().click();
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
