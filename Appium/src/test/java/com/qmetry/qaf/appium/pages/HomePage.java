package com.qmetry.qaf.appium.pages;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator="home.homeheader.text")
	QAFWebElement homeheader;
	
	@FindBy(locator="home.search.text")
	QAFWebElement serachbox;
	
	@FindBy(locator="home.runningshoe.img")
	QAFWebElement serachshoe;
	
	@FindBy(locator="home.womenshoe.text")
	QAFWebElement womenshoe;
	
	@FindBy(locator="home.womenheader.text")
	QAFWebElement womenshoeheader;
	
	@FindBy(locator="home.filter.text")
	QAFWebElement filter;
	
	@FindBy(locator="home.filter2.text")
	QAFWebElement filter1;

	@FindBy(locator="home.format.link")
	QAFWebElement format;

	@FindBy(locator="home.paperback.link")
	QAFWebElement papaerback;

	@FindBy(locator="home.searchresult.text")
	QAFWebElement searchresult;
	
	@FindBy(locator="home.harpatop.text")
	QAFWebElement harpatop;
	
	@FindBy(locator="home.topsize.text")
	QAFWebElement topsize;
	
	@FindBy(locator="home.addtocart.btn")
	QAFWebElement addtocart;
	
	@FindBy(locator="home.addtocartmsg.text")
	QAFWebElement addtocartmsg;
	
	@FindBy(locator="home.viewcarttop.text")
	QAFWebElement viewcarttop;
	
	@FindBy(locator="home.searchproduct.img")
	QAFWebElement searchproduct;
	
	@FindBy(locator="home.lipstick.link")
	QAFWebElement lipstick;
	
	@FindBy(locator="home.cartpage.text")
	QAFWebElement cartpage;
	
	@FindBy(locator="home.sort.text")
	QAFWebElement sort;
	
	@FindBy(locator="home.pricelowtohigh.text")
	QAFWebElement pricelowtohigh;
	
	public QAFWebElement getPriceLowToHigh()
	{
		return pricelowtohigh;
	}
	public QAFWebElement getSort()
	{
		return sort;
	}
	
	public QAFWebElement getCartPage()
	{
		return cartpage;
	}
	public QAFWebElement getLipstick()
	{
		return lipstick;
	}
	
	public QAFWebElement getSearchProduct()
	{
		return searchproduct;
	}
	
	public QAFWebElement getHarpaTop()
	{
		return harpatop;
	}
	
	public QAFWebElement getTopSize()
	{
		return topsize;
	}
	
	public QAFWebElement getAddToCart()
	{
		return addtocart;
	}
	
	public QAFWebElement getAddToCartMsg()
	{
		return addtocartmsg;
	}
	
	public QAFWebElement getViewCartTop()
	{
		return viewcarttop;
	}
	public QAFWebElement getSearchResult()
	{
		return searchresult;
	}
	public QAFWebElement getFilter()
	{
		return filter;
	}
	public QAFWebElement getFilter1()
	{
		return filter1;
	}
	public QAFWebElement getFormat()
	{
		return format;
	}
	public QAFWebElement getPapaerBack()
	{
		return papaerback;
	}
	public QAFWebElement getWomenShoeHeader()
	{
		return womenshoeheader;
	}
	public QAFWebElement getWomenShoe()
	{
		return womenshoe;
	}
	
	public QAFWebElement getSearchBox()
	{
		return serachbox;
	}
	
	public QAFWebElement getSearchShoe()
	{
		return serachshoe;
	}
	public QAFWebElement getHomeHeader()
	{
		return homeheader;
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
