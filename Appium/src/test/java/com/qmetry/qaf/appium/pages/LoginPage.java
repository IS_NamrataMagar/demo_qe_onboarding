package com.qmetry.qaf.appium.pages;

import com.qmetry.qaf.appium.beans.CreateAccountBean;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class LoginPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator="login.loginheader.text")
	QAFWebElement loginheader;
	
	@FindBy(locator="login.createaccount.radio")
	QAFWebElement createaccountradio;
	
	@FindBy(locator="login.name.text")
	QAFWebElement name;
	
	@FindBy(locator="login.phonenumber.text")
	QAFWebElement phone;
	
	@FindBy(locator="login.emailid.text")
	QAFWebElement email;
	
	@FindBy(locator="login.password.text")
	QAFWebElement password;
	
	@FindBy(locator="login.verifymobile.btn")
	QAFWebElement verifymobilebtn;
	
	@FindBy(locator="login.mobileverify.text")
	QAFWebElement mobileverifyheader;
	
	@FindBy(locator="login.verifymsg.text")
	QAFWebElement verifyregisterusermsg;
	
	@FindBy(locator="login.mobileemail.text")
	QAFWebElement emailmobilenumber;
	
	@FindBy(locator="login.continue.btn")
	QAFWebElement continuebtn;
	
	@FindBy(locator="login.header.text")
	QAFWebElement header;
	
	@FindBy(locator="login.loginsubmit.btn")
	QAFWebElement loginsubmit;
	
	public QAFWebElement getEmailMobileNumber()
	{
		return emailmobilenumber;
	}
	
	public QAFWebElement getContinueBtn()
	{
		return continuebtn;
	}
	
	public QAFWebElement getLoginHeader()
	{
		return loginheader;
	}
	
	public QAFWebElement getVerifyRegisterUserMsg()
	{
		return verifyregisterusermsg;
	}
	
	
	public QAFWebElement getMobileVerifyHeader()
	{
		return mobileverifyheader;
	}
	
	public QAFWebElement getCreateAccountRadio()
	{
		return createaccountradio;
	}
	
	public QAFWebElement getName()
	{
		return name;
	}
	
	public QAFWebElement getPhoneNumber()
	{
		return phone;
	}
	
	public QAFWebElement getEmailId()
	{
		return email;
		
	}
	
	public QAFWebElement getPassword()
	{
		return password;
	}
	
	public QAFWebElement getVerifyMobileBtn()
	{
		return verifymobilebtn;
	}
	
	public QAFWebElement getHeaderLogin()
	{
		return header;
	}
	
	public QAFWebElement getLoginSubmit()
	{
		return loginsubmit;
	}
	
	
	public void clickOnCreateAccount()
	{
		getCreateAccountRadio().click();
	}
	
	public void fillDetailsForCreateAccount()
	{
		CreateAccountBean createacc = new CreateAccountBean();
		createacc.fillFromConfig("user2.data");
		createacc.fillUiElements();
	}
	
	public void fillDetailsForAlreadyExistUser(String mobileemail,String pass)
	{
		getEmailMobileNumber().sendKeys(mobileemail);
		getContinueBtn().click();
		getPassword().sendKeys(pass);
	}
	
	public void clickOnVerifyMobileBtn()
	{
		getVerifyMobileBtn().verifyPresent();
		getVerifyMobileBtn().click();
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
